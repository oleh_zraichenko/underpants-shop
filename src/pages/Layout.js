import Carousel from "react-bootstrap/Carousel";
import React from 'react';


const Layout = () => (
      <Carousel>
        <Carousel.Item>
        <img
          className="d-block w-100"
          src={"./img/images.jpeg"}
          alt="First slide"/>
        <Carousel.Caption>
          <h3>Акция</h3>
          <p>Вязанный лифчик pink not floyd. Скидка 3,5 %</p>
        </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
        <img
          className="d-block w-100"
          src={"./img/40UAH.jpeg"}
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>Розыгрыш!</h3>
          <p>Сертификат на 40 грн</p>
        </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
)

export default Layout;