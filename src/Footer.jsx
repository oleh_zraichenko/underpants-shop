import {Row, Col, Container} from 'react-bootstrap';
import React from 'react';

export default function Footer() {
    return(
        <footer>
            <Container>
            <Row>
            <Col class="col-lg-8">
                <h5 class="text-dark letter-spacing-1 mb-4 d-none d-lg-block">Покупателям</h5>
                <ul class="list-unstyled text-sm pt-2 pt-lg-0">
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" 
                href="https://www.joom.com/ru/category">Каталог</a></li>
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated"
                 href="https://www.joom.com/ru/faq">Поддержка</a></li>
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated"
                 href="https://medium.com/@viknesh2798/how-to-dynamically-filter-and-sort-the-json-arrays-with-custom-fields-in-react-app-9a4a2fde5914">Чо-то ещще</a></li>
                </ul>
            </Col>
            <Col>
                <h5 class="text-dark letter-spacing-1 mb-4 d-none d-lg-block">Партнерам</h5>
                <ul class="list-unstyled text-sm pt-2 pt-lg-0">
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" 
                href ="https://merchant.joom.com/">Cтать продавцом</a></li>
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" href = "https://www.joom.com/ru/terms">Условия пользования</a></li>
                </ul>
            </Col>
            <Col>
                <h5 class="text-dark letter-spacing-1 mb-4 d-none d-lg-block">Компания</h5>
                <ul class="list-unstyled text-sm pt-2 pt-lg-0">
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" href="https://www.joom-group.com/en">О Компании</a></li>
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" 
                href="https://www.joom.com/ru/ipr-protection">Правообладателям</a></li>
                </ul>
                <h5 class="text-dark letter-spacing-1 mb-4 d-none d-lg-block">Компании Joom Group</h5>
                <ul class="list-unstyled text-sm pt-2 pt-lg-0">
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" href="https://joomlogistics.com/">Joom Logistics</a></li>
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" href="https://joompay.com/">Joompay</a></li>
                <li class="mb-2"><a class="text-decoration-none text-muted text-hover-dark link-animated" href="https://onfy.de/">Onfy</a></li>
                </ul>
            </Col>
            </Row>
            </Container>
        </footer>
    )
}