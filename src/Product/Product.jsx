import { Card, Button }  from 'react-bootstrap';
import React from 'react';
import products from './data.json'


export default function Product(props) {
  const productList = products.filter(products => products.sex === props.sex).map((product) =>
  <Card key = {product.id} style={{ width: '18rem' }}>
    <Card.Img variant="top" src= {product.image} />
      <Card.Body>
        <Card.Title>{product.name}</Card.Title>
        <Card.Text>
          {product.description}<br/>
          {product.price}
        </Card.Text>
        <Button variant="primary">Добавить в корзину</Button>
      </Card.Body>
    </Card>);
  return productList;
}
