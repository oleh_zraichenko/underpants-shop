import './App.css';
import React from 'react';
import Layout from './pages/Layout';
import Men from './pages/Men';
import Wimen from './pages/Wimen';
import NoPage from './pages/NoPage';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Footer from './Footer';
import HeaderNav from './Navbar';




const App = () => (
  <div className="App">
    <BrowserRouter  basename="/underpants-shop/">
    <HeaderNav />
      <Routes>
        <Route path="/" element={<Layout />}/>
          <Route path="/men" element={<Men />} />
          <Route path="/wimen" element={<Wimen />} />
          <Route path="/*" element={<NoPage />} />
      </Routes>
      <Footer />
    </BrowserRouter>
  </div>  
  );

export default App;
