import Container from 'react-bootstrap/Container';
import { Navbar, Nav } from 'react-bootstrap';
import React from 'react';
import {LinkContainer} from 'react-router-bootstrap'

function HeaderNav() {
  return (
    <Navbar bg="light" expand="lg" fixed = "top">
      <Container>
        <LinkContainer to="/">
         <Navbar.Brand> Магазин трусов</Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <LinkContainer to="/">
             <Nav.Link>Главная</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/men">
              <Nav.Link>Мужчинам</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/wimen">
              <Nav.Link>Женщинам</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default HeaderNav;

